<?php
include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.rt.php');



?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		
		<meta name="developed-by" content="Squeegee, http://sq.gg">
		
		<?php $this->seoHead(); ?>
		
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800,Libre+Baskerville:400,700' rel='stylesheet' type='text/css'>	
			
		<link rel="stylesheet" href="/site/css/lightbox.css">		
		<link href="/site/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="/site/css/style.css">
		
		<script src="/site/js/jquery.min.js"></script>
        <!--
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=&sensor=false"></script>
		<script src="/site/js/map.js"></script>
        -->
		
		<?php $this->pageToolsHead(); ?>
		<!-- -->
		<?php if (is_array($headers)) print implode("\r\n", $headers); ?>
		<!-- -->
                
	</head>
	<body id="page-<?php print strtolower(preg_replace(array("/[^a-zA-Z0-9 ]+/", "/[ ]+/"), array("", "-"), $this->page_info['page_title'])); ?>" class="template-<?php print $this->page_info['template_id']; ?> section-<?php print $this->page_info['section_id']; ?>">
		<?php $this->pageTools(); ?>
		
		<span class="glyphicon glyphicon-menu-hamburger nav-opener"></span>
		<header class="header white-block">
			<div class="container">
				<h1><a href=""><span class="hide"><?php print $this->page_info['page_title']; ?></span></a></h1>
				<h2 class="hide"><?php print SITE_TITLE; ?></h2>
				<nav class="nav inline-list">
					<$menu=default>
				</nav>
			</div>
		</header>
		<section class="sliderBar">
			<?php controller::sqgslider( controller::getSlides( $this->page_info['id'] ) ); ?>
		</section>
		<div class="page-content white-block">
			<div class="container">
				<$region=main-content>
				
				<div class="row">
					<div class="col-sm-24 home-property-experts">
						<$region=home-property-experts>
					</div>
					<div class="col-sm-24 home-commercial-experts">
						<$region=home-commercial-experts>
					</div>
				</div>
				
				<$region=secondary-content>
				
			</div>
		</div>
		
		
		
		
		
		
		
		
	
		<!-- ============================================================================================
					START PAGE CONTENT
		==============================================================================================-->
		

		