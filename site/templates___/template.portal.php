<?php
include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.cal.php');
cal::checkLogin();


$portal = $_SESSION['authorised'];
$sections = cal::pageContent();

include_once (HEADER_PAGE);
#print "<pre>".print_r($portal,1)."</pre>";
if($portal['homeurl']!=$_GET['home_url'])
{
	header("location: http://".$_SERVER['HTTP_HOST']);
	exit;
}
?>	


<div class="page-content portal-content">
	<div class="container">
		<h2 class="block-title"><?php print $portal['company']; ?></h2>
<?php
switch($portal['homeurl'])
{
	case "quadra":
?>
<div class="row">
	<div class="col col-sm-4">
		<div class="well">
			<h3>Document downloads</h3>
			<ul>
				<li><a href="/site/userfiles/File/LZWrgu83AVUkwm78/quadra.pdf">Clearsite Associates and Quadra</a></li>
			</ul>
		</div>
	</div>
	<div class="col col-sm-4">
		<div class="well">
			<h3>Contacts</h3>
			<dl>
				<dt>James Martland</dt>
				<dd><a href="mailto:james@clearsiteassociates.com">james@clearsiteassociates.com</a></dd><dd>Skype: <em>clearsiteltd</em></dd>
				<dt>Simon Bolstridge</dt>
				<dd><a href="mailto:simon@clearsiteassociates.com">simon@clearsiteassociates.com</a></dd><dd>Skype: <em>simonbolstridge</em></dd>
				<dt>Tom Bradbury</dt>
				<dd><a href="mailto:tom@clearsiteassociates.com">tom@clearsiteassociates.com</a></dd><dd>Skype: <em>tbradbury</em></dd>
			</dl>
		</div>

	</div>
	<div class="col col-sm-4"></div>
</div>


<?php
		break;
}?>	
	</div>
</div>


<?php include_once (FOOTER_PAGE);