<?php
include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.cal.php');
cal::checkLogin();


if(isset($_GET['submitted']))
{


$opts = array (
  'name' => 'Name',
  'email' => 'Email',
  'telephone' => 'Telephone',
  'comments' => 'Comments / enquiry',
  'projectinmind' => 'Project in mind?',
  'projecttitle' => 'Project title',
  'projectinfo' => 'Project info',
  'projectbudget' => 'Project budget',
);

$d="<div style='font-family: sans-serif; font-size: 12px;'>
<p>The following <strong>contact us</strong> request has just been submitted:</p>
	<table cellpadding=5 style='border-collapse: collapse; font-family: sans-serif;font-size:12px; width: 100%;'>";
	foreach($opts as $k=>$v)
	{
		$d.="\n<tr><th valign=top align=left style='width: 220px; border: 1px solid #ccc;'>";
		if(!is_array($v))
		{
			$d.=$v."</th><td style='border: 1px solid #ccc;'>".nl2br(htmlentities($_POST[$k],ENT_QUOTES,"UTF-8"))."</td></tr>";
		}else{
			$d.=$v['title']."</th><td valign=top style='border: 1px solid #ccc;'>";
			$sopts=array();
			foreach($v['opts'] as $ok=>$o)
			{
				if(!empty($_POST[$k][$ok]))
				{
					$sopts[] = "- ".htmlentities($o,ENT_QUOTES,"UTF-8");
				}
			}
			$d.=implode("<br />",$sopts)."</td></tr>";
		}
		#$d .= "<td>";
	}
	$d.="\n</table>
</div>";

#print "<pre>".print_r($d,1)."</pre>";exit;

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

#$headers .= 'To: <'.EMAIL.'>' . "\r\n";
$headers .= 'From: Clearsite Associates <noreply@clearsiteassociates.com>' . "\r\n";
//$headers .= 'Bcc: huntestates@t.sq.gg';

$to = EMAIL;
$subject = "Clearsite Associates - Contact form";

$spoofsent=true;

if(@mail($to, $subject, $d, $headers) || $spoofsent)
{
	$_SESSION['emailed'][1]=1;
}else{
	$_SESSION['emailed'][1]=0;
}
$_SESSION['emailed']['debug_1']=$d;

$redirect = "http://".$_SERVER['HTTP_HOST']."/#contact";
#print "<pre>".print_r($_SERVER,1)."</pre>";
#exit($redirect);
header("location: ".$redirect);
exit();
}

$slides = cal::slides();
#print "<pre>".print_r($slides,1)."</pre>";

include_once (HEADER_PAGE);
?>	

		


			<div class="caSlider">
				<div class="caBlurbs">
					<div class="container">
						<div class="caBlurbHolder">
							<?php foreach($slides as $s) {?>
							<div class="caBlurb" <?php controller::editLink( '/site/admin/slides.php?page_id=1&id=' . $s['id'] ); ?>>
								<p><?php echo $s['line_1']?></p>
								<a href="<?php echo $s['url']?>" class="btn"><?php echo $s['button_text']?></a>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="caSlides">
					<?php foreach($slides as $s) {?>
					<div class="caSlide">
						<div class="caImg" style="background-image: url(/site/userfiles/Image/slides/<?php echo $s['image']?>); ">
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			
			
			
			
			
			
			
			
			<div class="portfolio" id="portfolio">
				<div class="portfolio-item">
					<div class="portfolio-img">
						<$region=portfolio_img>
					</div>
					<div class="portfolio-blurb">
						<div class="container">
							<$region=portfolio>
						</div>
					</div>
				</div>
			</div>
			<div class="taglines">
				<div class="container">
					<div class="col-left">
						<img src="/site/img/logo.green.svg" alt="">
						<h3>CLEARSITE ASSOCIATES</h3>
						<p>MAKE WEBSITES THAT WORK</p>
						<a href="/#contact" class="btn btn-green">Get in touch</a>
					</div>
					<div class="col-right">
						<img src="/site/img/time.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="testimonials">
				<div class="container">
					<$region=main1>
					<a href="/#getting-started" class="btn btn-inline">Start your next web project with Clearsite Associates</a>
				</div>
			</div>
			<div class="about-us" id="about-us">
				<div class="container">
					<h2 class="block-title" data-scroll-reveal>ABOUT US</h2>
					<div class="row">
						
<?php
$team = cal::team();
foreach($team as $t){
?>
						<div class="col-sm-4 staff-profile slideUp" <?php controller::editLink( '/site/admin/team.php?team_id=' . $t['team_id'] ); ?>>
							<div class="staff-img">
								<img src="/site/userfiles/Image/team/<?php echo $t['imagename']?>" alt="">
							</div>
							<h3><?php echo $t['name']?></h3>
							<h4><?php echo $t['position']?></h4>
							<div class="staff-bio">
								<?php echo $t['content']?>
							</div>
							<div class="social">
								<a href=""><img src="" alt=""></a>
							</div>
						</div>

<?php } ?>
						
					</div>
				</div>
			</div>
		


<div class="gmap"></div>
			<div class="contact-form" id="contact">
				<div class="container" id="getting-started">
<?php 
if(isset($_SESSION['emailed'][1]))
{
	#print "<pre>".print_r($_SESSION,1)."</pre>";

	if($_SESSION['emailed'][1]!=1)
	{
		$alertclass="danger";
		$alert = "We're sorry - there was an error submitting your request. Please contact us directly</a>.";
	}else{
		$alertclass="success";
		$alert = "Your request has been submitted. We will be in touch shortly. Thank you";
	}
	if(($_SERVER['REMOTE_ADDR'] == "127.0.0.1x") || ($_SERVER['REMOTE_ADDR'] == "80.3.105.105x"))
	{
		$debugemail = '<div class="alert alert-warning" role="alert"><p><strong>The following is the email created:</strong></p>'.$_SESSION['emailed']['debug_'.$_SERVER['REDIRECT_URL']].'</div>';
	}

	unset($_SESSION['emailed'][1]);
?>
<h2 class="block-title">THANKS</h2>
<div class="alert alert-<?php echo $alertclass?>" role="alert"><?php echo $alert?></div>

<?php
if(!empty($debugemail))
{
	echo $debugemail;
}

}else{ ?>
					
					<h2 class="block-title">GET IN TOUCH</h2>
					<form action="?submitted" method="POST" class="form">
						<div class="row">
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<label for="" class="sr-only"></label>
									<input type="text" class="form-control" placeholder="Your name" name="name">
								</div>
								<div class="form-group">
									<label for="" class="sr-only"></label>
									<input type="text" class="form-control" placeholder="Your email address" name="email">
								</div>
								<div class="form-group">
									<label for="" class="sr-only"></label>
									<input type="text" class="form-control" placeholder="Your contact telephone number" name="telephone">
								</div>
								<div class="form-group">
									<label for="" class="sr-only"></label>
									<textarea id="" class="form-control" placeholder="Your comments and/ or enquiry" name="comments"></textarea>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="form-group">
									<label><input type="checkbox" class="switch" name="projectinmind" value="Yes" checked="checked"> Do you have a project in mind?</label>
								</div>
								<div class="project-info">
									<div class="form-group">
										<label for="" class="sr-only"></label>
										<input type="text" class="form-control" placeholder="Project title" name="projecttitle">
									</div>
									<div class="form-group">
										<label for="" class="sr-only"></label>
										<textarea id="" class="form-control" placeholder="Project information" name="projectinfo"></textarea>
									</div>
									<div class="form-group">
										<label for="" class="sr-only"></label>
										<input type="text" class="form-control" placeholder="Do you have a budget in mind?" name="projectbudget">
									</div>
								</div>
							</div>
							<div class="col-sm-12 btn-holder">
								<button class="btn btn-inline">Submit</button>
							</div>
						</div>
					</form>

<?php } ?>
				</div>
			</div>

<?php include_once (FOOTER_PAGE);