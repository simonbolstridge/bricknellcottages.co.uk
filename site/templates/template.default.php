<?php
include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.bc.php');

$banners = bc::getBanners($this->page_info['id']);

include_once (HEADER_PAGE);
?>	
<div class="divide60"></div>
<div class="container template-default-container">
	<div class="row">
		<div class="col-sm-6 main-content-container">
			<h3 class="heading"><$string=page_title></h3>
			<span class="tagline"><$string=tagline></span>
			<$region=main_content>
		</div>
		<div class="col-sm-6">
			<$region=content-02>
		</div>
	</div>
</div>
<div class="divide60"></div>



<?php include_once (FOOTER_PAGE);