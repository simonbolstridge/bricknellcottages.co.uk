<?php
include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.bc.php');

$cottages = bc::getCottages();
$banners = bc::getBanners($this->page_info['id']);
	
include_once (HEADER_PAGE);
?>	

<div class="divide60"></div>
<div class="container">
	<div class="row">
		<div class="col-md-6 margin20">
			<h3 class="heading"><$string=opening_title></h3>
			<$region=opening_blurb>
		</div>
		<div class="col-md-6 ">
			<div class="video-holder">
				<span class="button round outline-translucent-mix text-uppercase">
				  <span class="fa fa-play-circle"></span>Watch video
				</span>
				<img src="/site/userfiles/Image/content/side-image.jpg" class="img-responsive" alt="">
			</div>
		</div>
	</div><!--about intro-->
	<div class="divide60"></div>
	<div class="row">
		<div class="col-md-12 margin20">
			<h3 class="heading">Our Cottages</h3>
		</div>
	</div>
	
	<div class="row cottage-summary-list">
		<?php foreach ($cottages as $cottage){ $photo = current($cottage['photos']); $cottage_url = sprintf ("/our-cottages/%s", $cottage['cottage_url']); ?>
		<div class="col-md-4 margin20 cottage-summary">
			<div class="team-wrap">
				<a href="<?php print $cottage_url; ?>" class="team-photo-holder"><img src="/site/userfiles/Image/cottages/<?php print $photo['photo_url']; ?>" class="img-responsive" alt=""></a>
				<h4><a href="<?php print $cottage_url; ?>"><?php print $cottage['cottage']; ?></a></h4>
				<span><?php print $cottage['cottage_tagline']; ?></span>
				<?php print $cottage['cottage_introduction']; ?>
				<a href="<?php print $cottage_url; ?>" class="btn btn-theme-bg btn-lg">Find out more</a>
				
			</div><!--team-wrap-->
		</div><!--team col-->			
		<?php } ?>
	</div>
</div>



<?php include_once (FOOTER_PAGE);