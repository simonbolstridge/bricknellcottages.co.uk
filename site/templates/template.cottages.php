<?php
include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.bc.php');

$cottages = bc::getCottages();
$cottage_url = sprintf ("%s", $_GET['cottage_url']);
if ($cottage_url && $cottages[$cottage_url])
{
	$cottage = $cottages[$cottage_url];
	$bannerSql = sprintf ("
		SELECT *
		FROM site_slides
		WHERE cottage_id = %d
		ORDER BY orderby DESC
	",
		$cottage['cottage_id']
	);
	$banners = bc::getBanners('', $bannerSql);
}else{
	$banners = bc::getBanners($this->page_info['id']);
}


include_once (HEADER_PAGE);
?>	





<?php if ($cottage){ ?>
<!--=========================================================
		START COTTAGE
===========================================================-->
<div class="divide60"></div>
<div class="container cottage-full">
	<div class="row">
		<div class="col-sm-6">
			<h3 class="heading"><?php print $cottage['cottage']; ?></h3>
			<span class="tagline"><?php print $cottage['cottage_tagline']; ?></span>
			<div class="cottage-blurb">
				<?php print $cottage['cottage_full_details']; ?>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="/site/userfiles/vt/<?php print $cottage['vt_dir']; ?>/build.html"></iframe>
			</div>
			<div id="grid" class="row cottage-gallery">
				<?php foreach ($cottage['photos'] as $photo){ ?>
				<div class="mix col-sm-4 margin30">
					<div class="item-img-wrap ">
						<img src="/site/userfiles/Image/cottages/<?php print $photo['photo_url']; ?>" class="img-responsive" alt="workimg">
						<div class="item-img-overlay">
							<a href="/site/userfiles/Image/cottages/<?php print $photo['photo_url']; ?>" class="show-image">
								<span></span>
							</a>
						</div>
					</div> 
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<!--=========================================================
		END COTTAGE
===========================================================-->
<?php } else { ?>
<!--=========================================================
		START COTTAGES
===========================================================-->
<div class="divide60"></div>
<div class="container cottages">
	<div class="row">
		<div class="col-md-12 margin20">
			<h3 class="heading">Our Cottages</h3>
			<div class="cottages-introduction">
				<$region=cottages-intro>
			</div>
		</div>
	</div>
	
	<div class="row cottage-summary-list">
		<?php foreach ($cottages as $cottage){ $photo = current($cottage['photos']); $cottage_url = sprintf ("/our-cottages/%s", $cottage['cottage_url']); ?>
		<div class="col-md-4 margin20 cottage-summary">
			<div class="team-wrap">
				<a href="<?php print $cottage_url; ?>" class="team-photo-holder"><img src="/site/userfiles/Image/cottages/<?php print $photo['photo_url']; ?>" class="img-responsive" alt=""></a>
				<h4><a href="<?php print $cottage_url; ?>"><?php print $cottage['cottage']; ?></a></h4>
				<span><?php print $cottage['cottage_tagline']; ?></span>
				<?php print $cottage['cottage_introduction']; ?>
				<a href="<?php print $cottage_url; ?>" class="btn btn-theme-bg btn-lg">Find out more</a>
			</div><!--team-wrap-->
		</div><!--team col-->			
		<?php } ?>
	</div>
</div>
<!--=========================================================
		END COTTAGES
===========================================================-->
<?php } ?>




<?php include_once (FOOTER_PAGE);