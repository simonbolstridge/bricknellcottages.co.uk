<?php

# ini_set('session.use_trans_sid', '0');
$char = strtoupper(substr(str_shuffle('abcdefghjkmnpqrstuvwxyz'), 0, 4));
$str = rand(1, 7) . rand(1, 7) . $char;
$_SESSION['captcha_id'] = $str;


include_once (CONTROLLERS . 'queryi.php');
include_once (CONTROLLERS . 'controller.php');
include_once (CONTROLLERS . 'controller.bc.php');

$banners = bc::getBanners($this->page_info['id']);

/* QUICK AND LAZY FORM SUBMISSION! SHAME ON ME! */
if( isset($_POST['name']) && strtoupper($_POST['captcha']) == $_SESSION['captcha_id'] )
{
	$to = 's@sq.gg'; // Replace with your email	
	$subject = 'Message from website'; // Replace with your $subject
	$headers = 'From: ' . $_POST['email'] . "\r\n" . 'Reply-To: ' . $_POST['email'];	
	
	ob_start();
	?>
	<h2>Another message from the website</h2>
	<table>
		<?php foreach ($_POST as $key=>$var){ $var = sprintf ("%s", $_POST[$key]); ?>
		<tr>
			<td><?php print $key; ?></td>
			<td><?php print $var ? nl2br($var) : '&nbsp;'; ?></td>
		</tr>
		<?php } ?>
	</table>
	<table>
		<tr>
			<td>Date/ Time</td>
			<td><?php print date("Y-m-d H:i:s"); ?></td>
		</tr>
		<tr>
			<td>Remote IP</td>
			<td><?php print $_SERVER['REMOTE_ADDR']; ?></td>
		</tr>
		<tr>
			<td>Web URL</td>
			<td><?php print $_SERVER['REQUEST_URI']; ?></td>
		</tr>
		<tr>
			<td>Agent</td>
			<td><?php print $_SERVER['HTTP_USER_AGENT']; ?></td>
		</tr>
	</table>
	<?php
	$message = ob_get_clean();
	/*$message = 'Name: ' . $_POST['name'] . "\n" .
	           'E-mail: ' . $_POST['email'] . "\n" .
	           'Subject: ' . $_POST['subject'] . "\n" .
	           'Message: ' . $_POST['message'];*/
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	mail($to, $subject, $message, $headers);	
	if( $_POST['copy'] == 'on' )
	{
		mail($_POST['email'], $subject, $message, $headers);
	}
	exit();
}
/* END FORM SUBMISSION */

$contact = true;



include_once (HEADER_PAGE);
?>	

        <div id="map-canvas"></div>
        <div class="divide60"></div>
		
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-offset-1 margin30 sky-form-columns contact-sky-option">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="center-heading">
                                <h2>Contact us</h2>
                                <span class="center-line"></span>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </div>
                        </div>                   
                    </div><!--center heading row-->
                    <div class="row margin40">
                        <div class="col-md-4 margin30">
                            <div class="contact-option-circle">
                                <i class="fa fa-home"></i>
                                <p>
                                    Bricknell Cottages, Pear Tree Farm, Fangfoss, York YO41 5QH
                                </p>
                            </div><!--contact option circle-->
                        </div><!--contact option detail box col-->
                         <div class="col-md-4 margin30">
                            <div class="contact-option-circle">
                                <i class="fa fa-phone"></i>
                                <p>
                                    tel: 01759 368 588<br>
                                    fax: 01759 368 699
                                </p>
                              
                            </div><!--contact option circle-->
                        </div><!--contact option detail box col-->
                         <div class="col-md-4 margin30">
                            <div class="contact-option-circle">
                                <i class="fa fa-envelope"></i>
                                <p>
                                <a href="mailto:enquiries@bricknellcottages.co.uk">enquiries@bricknellcottages.co.uk</a><br><br></p>
                            </div><!--contact option circle-->
                        </div><!--contact option detail box col-->
                    </div>
                     <p class="margin30">
                            <b>Get in touch!</b> We'd love to hear from you.
                        </p>
                        <form action="" method="post" id="sky-form" class="sky-form sky-form-columns">
				<fieldset>					
					<div class="row">
						<section class="col col-6">
							<label class="label">Name</label>
							<label class="input">
								<i class="icon-append fa fa-user"></i>
								<input type="text" name="name" id="name">
							</label>
						</section>
						<section class="col col-6">
							<label class="label">E-mail</label>
							<label class="input">
								<i class="icon-append fa fa-envelope-o"></i>
								<input type="email" name="email" id="email">
							</label>
						</section>
					</div>
					
					<section>
						<label class="label">Subject</label>
						<label class="input">
							<i class="icon-append fa fa-tag"></i>
							<input type="text" name="subject" id="subject">
						</label>
					</section>
					
					<section>
						<label class="label">Message</label>
						<label class="textarea">
							<i class="icon-append fa fa-comment"></i>
							<textarea rows="4" name="message" id="message"></textarea>
						</label>
					</section>
					
					<section>
						<label class="label">Enter characters below:</label>
						<label class="input input-captcha">
                        	<img src="/site/assan/sky-form/captcha/image.php?<?php echo time(); ?>" width="100" height="35" alt="Captcha image" />
							<input type="text" maxlength="6" name="captcha" id="captcha">
						</label>
					</section>
					
					<section>
						<label class="checkbox"><input type="checkbox" name="copy"><i></i>Send a copy to my e-mail address</label>
					</section>
				</fieldset>
				
				<footer>
					<button type="submit" class="btn btn-theme-bg btn-lg ">Send message</button>
				</footer>
				
				<div class="message">
					<i class="fa fa-check"></i>
					<p>Your message was successfully sent!</p>
				</div>
			</form>	
        						
                </div>

            </div>
        </div><!--contact advanced container end-->
        <div class="divide60"></div>

<?php include_once (FOOTER_PAGE);