		
<!-- ============================================================================================
			END PAGE CONTENT
==============================================================================================-->	
		
			
			
		

	
	 <footer id="footer">
		<div class="container">
			<div class="row" style="padding-bottom: 40px; ">
				<div class="col-sm-4">
					<div class="footer-col">
						<$global=footer-01>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="footer-col">
						<$global=footer-02>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="footer-col">
						<$global=footer-03>
					</div>
				</div>
			</div>
		</div>
	</footer>	
	
	<!-- AddThis Smart Layers BEGIN -->
	<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e2528616f5d9a0"></script>
	<script type="text/javascript">
	  addthis.layers({
		 'theme' : 'transparent',
		 'share' : {
			'position' : 'left',
			'numPreferredServices' : 5
		 }, 
		 'follow' : {
			'services' : [
			  {'service': 'facebook', 'id': 'pages/Bricknell-Cottages-Fangfoss/120177731395863'}
			]
		 }   
	  });
	</script>
	<!-- AddThis Smart Layers END -->

	
	<?php
		if (!$_SESSION['auth']){
		/*
		----------------------------------------------------	
		# 		GOOGLE ANALYTICS
		----------------------------------------------------	
		*/
		?>
		
		
		
		<?php
		/*
		----------------------------------------------------	
		# 		END GOOGLE ANALYTICS
		----------------------------------------------------	
		*/
		}
		?>
		
		<?php
		require_once LIB_DIR . "classes/jsmin.php";
		define ("MODE", "DEVELOPMENT");
		$jsmin = new jsmin();
		$jsmin->files[] = SITE_DIR . 'js/bootstrap.min.js';
		$jsmin->files[] = SITE_DIR . 'js/hammer-v2.0.3.min.js';
		$jsmin->files[] = SITE_DIR . 'js/jquery.parallax-1.1.3.js';	
		$jsmin->files[] = SITE_DIR . 'js/transit.min.js';
		#$jsmin->files[] = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyBvTgpWfoTUerpfWdOIRV6IkdHol_UmcsY&sensor=false';
		$jsmin->files[] = SITE_DIR . 'js/bootstrap-switch.min.js';
		$jsmin->files[] = SITE_DIR . 'js/core.js';
		$jsmin->write(SITE_DIR . '/js/jsmin.js');
		?>
		<script src="/site/js/jsmin.js"></script>
		
		
		
		
        <script src="/site/assan/js/jquery-migrate.min.js"></script> 
        <!--bootstrap js plugin-->
        <script src="/site/assan/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>       
        <!--easing plugin for smooth scroll-->
        <script src="/site/assan/js/jquery.easing.1.3.min.js" type="text/javascript"></script>
        <!--sticky header-->
        <script type="text/javascript" src="/site/assan/js/jquery.sticky.js"></script>
        <!--flex slider plugin-->
        <script src="/site/assan/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <!--parallax background plugin-->
        <script src="/site/assan/js/jquery.stellar.min.js" type="text/javascript"></script>
        <!--digit countdown plugin-->
        <script src="/site/assan/js/waypoints.min.js"></script>
        <!--digit countdown plugin-->
        <script src="/site/assan/js/jquery.counterup.min.js" type="text/javascript"></script>
        <!--on scroll animation-->
        <script src="/site/assan/js/wow.min.js" type="text/javascript"></script> 

        <script src="/site/assan/js/jquery.isotope.min.js" type="text/javascript"></script>
        <!--image loads plugin -->
        <script src="/site/assan/js/jquery.imagesloaded.min.js" type="text/javascript"></script>
        <!--owl carousel slider-->
        <script src="/site/assan/js/owl.carousel.min.js" type="text/javascript"></script>
        <!--popup js-->
        <script src="/site/assan/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <!--you tube player-->
        <script src="/site/assan/js/jquery.mb.YTPlayer.min.js" type="text/javascript"></script>        
        <!--customizable plugin edit according to your needs-->
        <script src="/site/assan/js/custom.js" type="text/javascript"></script>

        <!--revolution slider plugins-->
        <script src="/site/assan/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="/site/assan/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="/site/assan/js/revolution-custom.js" type="text/javascript"></script>
        <script src="/site/assan/js/isotope-custom.js" type="text/javascript"></script>
		
		
		<?php if ($contact){ ?>
		        <!--sky form plugin js-->
        <script src="/site/assan/sky-form/js/jquery.form.min.js" type="text/javascript"></script>
        <script src="/site/assan/sky-form/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="/site/assan/js/sky-forms-custom.js" type="text/javascript"></script>
        <!--gmap js-->
        <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=&sensor=false"></script>-->
      	 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5iJeID_Pyxx6HJQ-u89vrlMZx6fMVGm8&sensor=true"></script>
        <script type="text/javascript">
            var myLatlng;
            var map;
            var marker;

            function initialize() {
                myLatlng = new google.maps.LatLng(53.970208,-0.831635);

                var mapOptions = {
                    zoom: 13,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    draggable: false
                };
                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                var contentString = '<p style="line-height: 20px;"><strong>assan Template</strong></p><p>Vailshali, assan City, 302012</p>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: 'Marker'
                });

                google.maps.event.addListener(marker, 'click', function () {
                    //infowindow.open(map, marker);
					window.open('https://maps.google.co.uk/maps?q=York+YO41+5QH&hl=en&sll=53.970208,-0.831635&t=m&z=16');
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

		<?php } ?>
		
	</body>
</html>