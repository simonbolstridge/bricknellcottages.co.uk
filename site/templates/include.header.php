<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		
		<meta name="developed-by" content="Squeegee Design - http://sq.gg/">
		
		<?php $this->seoHead(); ?>
		
		
		
		
		
		<!--
        <link href="/site/assan/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		
        <link href="/site/assan/css/style.css" rel="stylesheet" type="text/css" media="screen">
		
        <link href="/site/assan/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		
        <link href="/site/assan/css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
		
        <link href="/site/assan/css/animate.css" rel="stylesheet" type="text/css" media="screen"> 
        <link href="/site/assan/rs-plugin/css/settings.css" rel="stylesheet" type="text/css" media="screen">
        <link href="/site/assan/css/rev-style.css" rel="stylesheet" type="text/css" media="screen">
        <link href="/site/assan/css/owl.carousel.css" rel="stylesheet" type="text/css" media="screen">
        <link href="/site/assan/css/owl.theme.css" rel="stylesheet" type="text/css" media="screen">
        <link href="/site/assan/css/yamm.css" rel="stylesheet" type="text/css">
        <link href="/site/assan/css/magnific-popup.css" rel="stylesheet" type="text/css">
		<link href="/site/assan/sky-form/css/sky-forms.css" rel="stylesheet">
		-->
	

		
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		<link href="/site/css/style.css" rel="stylesheet">
		
		<script src="/site/js/jquery.min.js"></script>
		<?php $this->pageToolsHead(); ?>
		
		
		
		
		<!-- -->
		<?php if (is_array($headers)) print implode("\r\n", $headers); ?>
		<!-- -->
        
		<style>
		</style>
		  
	</head>
	<body id="page-<?php print strtolower(preg_replace(array("/[^a-zA-Z0-9 ]+/", "/[ ]+/"), array("", "-"), $this->page_info['page_title'])); ?>" class="template-<?php print $this->page_info['template_id']; ?> section-<?php print $this->page_info['section_id']; ?> scrolled">
		<?php $this->pageTools(); ?>
		
		
		
		
		
<!-- ============================================================================================
			START PAGE CONTENT
==============================================================================================-->

		<div class="navbar navbar-default navbar-static-top yamm sticky" role="navigation">
            <div class="container">
				<div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1><a href="/"><span>Bricknell Cottages</span></span></a></h1>
                </div>
				<div class="navbar-collapse collapse">
					<$menu=default|class:nav navbar-nav navbar-right>				
				</div>
			</div>
		</div>
				
				
		<?php if ($banners){ ?>
        <article>
            <div>
                <div class="tp-banner-vertical" >
                    <ul>
						<?php foreach ($banners as $banner){ ?>
						<!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="" data-masterspeed="1000" data-thumb="img/construction/constro-bg-1.jpg"  data-saveperformance="off"  data-title="Slide">
                            <!-- MAIN IMAGE -->
                            <img src="/site/userfiles/Image/slides/<?php print $banner['image']; ?>"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption vertical-title lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="100"
                                 data-y="center" data-voffset="-70"
                                 data-speed="600"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 ><?php print $banner['line_1']; ?>
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption vertical-caption lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="100"
                                 data-y="center" data-voffset="20"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 ><?php print $banner['line_2']; ?>
                            </div>    
                            <!-- LAYER NR. 3 -->
							<?php if ($banner['url']){ ?>
                            <div class="tp-caption rev-buttons lfb ltt tp-resizeme"
                                 data-x="left" data-hoffset="100"
                                 data-y="center" data-voffset="110"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 ><a href="<?php print $banner['url']; ?>" class="btn btn-lg border-white"><?php print $banner['button_text'] ? $banner['button_text'] : 'LEARN MORE'; ?></a>
                            </div> 
							<?php } ?>
                        </li>
						<?php } ?>

                    </ul>	
                    <div class="tp-bannertimer"></div>	
                </div>
            </div>	
        </article>		<!--revolution vertical slider end-->
		<?php } ?>	
		<div class="clearfix"></div>
		 <div class="intro-text-1 book-online-form-block">
            <div class="container">
                <div class="row">
					<form action="" class="form form-inline booking-form">
						<div class="col-sm-3">
							<h4 class="animated slideInDown">BOOK ONLINE NOW!</h4>
						</div>
						<div class="col-sm-9">
							<div class="form-group">
								<label for="">ARRIVE</label>
								<select name="" id="" class="form-control">
									<option>Please select</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">DEPART</label>
								<select name="" id="" class="form-control">
									<option>Please select</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">ADULTS</label>
								<select name="" id="" class="form-control">
									<option>Please select</option>
								</select>
							</div>
							<div class="form-group">
								<button role="submit" class="btn btn-theme-bg btn-lg">BOOK NOW</button>
							</div>
						</div>
					</form>				
                </div>
            </div>
        </div><!--quote text end-->

