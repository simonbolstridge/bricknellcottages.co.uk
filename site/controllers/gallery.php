<?php
class gallery extends querytools {
	
	function __construct()
	{
		
	}
	
	function getPage()
	{
		
	}
	
	function getgalleryById($gallery_id)
	{
		$this->_unset();
		$this->_sql = sprintf ("
			SELECT *
			FROM site_gallery_photos
			
		",
			$gallery_id
		);
		return $this->getResultset();
	}
	
	function getAll()
	{
		$this->_unset();
		$this->_sql = "
			SELECT *
			FROM site_gallery_photos p
			LEFT JOIN site_galleries g
			ON g.id = p.gallery_id
			WHERE g.available = 1
		";
		$galleries = array();
		while ($this->nextrecord())
		{
			if ($this->r['gallery_id'])
			{
				if (!$galleries[$this->r['gallery_id']]) $galleries[$this->r['gallery_id']] = $this->r;
				$galleries[$this->r['gallery_id']]['photos'][] = $this->r;
			}
		}
		# print '<pre>'; print_r($galleries); exit();
		return $galleries;
	}
	
	public function getRandom($qty = 6, $page_bitwise = 0)
	{
		$this->_unset();
		$this->_sql = sprintf ("
			SELECT photo_id, photo, caption
			FROM site_gallery_photos p
			LEFT JOIN site_galleries g
			ON p.`gallery_id` = g.`id`
			WHERE p.`photo_available` = 1 AND g.`available` = 1 AND (p.specific_page & %d)
			ORDER BY RAND()
		",
			$page_bitwise
		);
		$return = $this->getResultset();
		if (empty($return))
		{
			$this->_unset();
			$this->_sql = sprintf ("
				SELECT photo_id, photo, caption
				FROM site_gallery_photos p
				LEFT JOIN site_galleries g
				ON p.`gallery_id` = g.`id`
				WHERE p.`photo_available` = 1 AND g.`available` = 1
				
				ORDER BY RAND()
				LIMIT %d
			",
				$qty
			);
			$return = $this->getResultset();
		}
		return $return;
	}
	
}