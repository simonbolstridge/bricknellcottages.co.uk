<?php

class queryi {
	
	private static $connection;
	
	public function getConnection()
	{
		if (!self::$connection) 
			if (!self::connect()) return false;
			return true;
	}
	
	public function connect()
	{
		self::$connection = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );
		if (mysqli_connect_errno())
		{
			print "Failed to connect to MySQL: " . mysqli_connect_error();
			return false;
		}
		return true;
	}
	
	public function query( $sql = '' )
	{
		if (!self::getConnection()) return false;
		return mysqli_query( self::$connection, $sql );
	}
	
	public function getResultset ( $sql = '', $key = '' )
	{
		if (!$resultset = self::query( $sql )) return array();
		
		while ( $row = mysqli_fetch_assoc( $resultset ) )
		{
			if ($key)
			{
				$return[ $row[$key] ]  = $row;
			}else{
				$return[]  = $row;
			}
		}
		
		return $return;
	}
		
}

