<?php
class services extends querytools {
	
	function __construct()
	{
		
	}
	
	function getService()
	{
		
	}
	
	function getServicesByBitwise($bitwise = 0)
	{
		$this->_unset();
		$this->_sql = sprintf ("
			SELECT *
			FROM services
			WHERE bitwise_operator & %d AND service_available = 1
			ORDER BY service_orderby DESC
		",
			$bitwise
		);
		return $this->bitwiseServices = $this->getResultset();
	}
	
	function getAll()
	{
		$this->_unset();
		$this->_sql = "
			SELECT *
			FROM services
			WHERE service_available = 1
			ORDER BY service_orderby DESC
		";
		return $this->services = $this->getResultset();
	}
	
}