<?php

class bc extends controller { /* BRICKNELL COTTAGES */
	
	public function getCottages()
	{
		$sql = "
			SELECT *
			FROM cottages c
			LEFT JOIN cottage_photos p
			ON c.`cottage_id` = p.`cottage_id`
			WHERE c.`cottage_active` = 1
			ORDER BY c.`cottage_orderby` DESC, c.`cottage_id` DESC, p.`photo_orderby` DESC, p.`photo_id` DESC
		";
		$results = self::getResultset($sql);
		foreach ($results as $photo)
		{
			if (!$cottages[$photo['cottage_url']]) 
			{
				$cottages[$photo['cottage_url']] = $photo;
				$titleBits = explode("-", $photo['cottage_name']);
				$cottages[$photo['cottage_url']]['cottage'] = $titleBits[0];
				$cottages[$photo['cottage_url']]['cottage_tagline'] = $titleBits[1];
			}
			$cottages[$photo['cottage_url']]['photos'][] = $photo;
		}
		return $cottages;
	}
	
	public function getBanners ($pageId='', $sql = '')
	{
		if (!$sql)
		{
			$sql = sprintf ("
				SELECT *
				FROM site_slides
				WHERE page_id = '%d'
				ORDER BY orderby DESC, id DESC
			",
				$pageId
			);
		}
		return self::getResultset( $sql );
	}
	
	public function checkLogin()
	{
		$path = sprintf ("%s", $_GET['path']);
		if ($path == "/logout") self::logout();
		
		$loginEmail = sprintf ("%s", $_POST['loginEmail']);
		$loginPassword = sprintf ("%s", $_POST['loginPassword']);
		if (!$loginEmail || !$loginPassword) return false;
		
		$sql = sprintf ("
			SELECT firstname, lastname, homeurl, email, company, company_id
			FROM clientlogin c
			LEFT JOIN clientcompanies cc
			ON c.company_id = cc.id
			WHERE c.email = '%s' AND c.password = '%s' AND c.valid = 1 AND cc.valid = 1
		",
			$loginEmail,
			md5($loginPassword)
		);
		$authorised = self::getResultset( $sql );
		if ($authorised) {
			$authorisedUser = current($authorised);
			
			$_SESSION['authorised'] = $authorisedUser;
			$return = array(
				'success'=>true,
				'redirect'=>'/portal/' . $authorisedUser['homeurl']
			);
		}else{
			$return = array(
				'error'=>true,
				'html'=>'<h3>Whoops.</h3><p>We don\'t appear to have any authorised access matching those credentials.</p>'
			);
		}
		self::json($return);
	}
	
	public function logout()
	{
		if ($_SESSION['authorised']) unset($_SESSION['authorised']);
		header ("location: /");
		exit();
	}
	
	public function pageContent()
	{
		
	}
	
	public function authArea()
	{
		if (!$_SESSION['authorised']) return self::loginLink();
		self::loginDetails();
	}
	
	public function loginLink()
	{
		if ($_SESSION['permission']) return false;
		?>
		<a href="/login" class="loginLink">Client Login <span class="glyphicon glyphicon-lock"></span></a>
		<?php
	}
	
	public function json( $str )
	{
		header('Content-Type: application/json');
		print json_encode ( $str );
		exit();
		
	}
	
	public function team()
	{

		$sql = "SELECT *
			FROM site_team
			ORDER BY orderer DESC";

		$team = self::getResultset( $sql );
		return $team;
	}

	public function slides()
	{

		$sql = "SELECT *
			FROM site_slides
			ORDER BY orderby DESC";

		$res = self::getResultset( $sql );
		return $res;
	}
	
	public function loginDetails()
	{
		$sql = sprintf ("
			SELECT page_url, page_title, 
			FROM clientpages
			WHERE company_id = %d
		",
			$_SESSION['authorised']['company_id']
		);
		$pages = self::getResultset( $sql );
		$li = '<ul>';
		foreach ($pages as $page){
			// $li .= sprintf ('<li><a href="/%s/%s">%s</a></li>', $_SESSION['authorised']['homeurl'], $page['page_url'], $page['page_title']);
		}
		$li .= '</ul>';
		?>
		<span class="a mobile-hide">Welcome <?php print $_SESSION['authorised']['firstname']; ?></span><a href="/portal/<?php print $_SESSION['authorised']['homeurl']; ?>"><?php print $_SESSION['authorised']['company']; ?></a><a href="/logout" class="logoutLink">Logout <span class="glyphicon glyphicon-lock"></span></a>
		<?php
	}
	
}