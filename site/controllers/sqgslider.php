<?php
class sqgslider {
	
	function __construct( $slides='', $id='' )
	{
		
		if (!is_array($slides) || empty($slides)) return false;
		?>
		<div class="sqgslider"<?php if ($id) print sprintf (' id="%s"', $id); ?>>
			
			<?php foreach ($slides as $slide){ ?>
			<div class="slide">
				
				<img src="" alt="">
				<p class="line_1"><?php print $slide['line_1']; ?></p>
				<p class="line_2"><?php print $slide['line_2']; ?></p>
				
				<?php if ($slide['url']){ ?>
				<a href=""><?php print $slide['button_text'] ? $slide['button_text'] : 'More info'; ?> <span class="glyphicon glyphicon-forward"></span></a>
				<?php } ?>
				
			</div>
			<?php } ?>
			
		</div>
		<?php
		
	}
	
}