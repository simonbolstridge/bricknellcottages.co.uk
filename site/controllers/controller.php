<?php


class controller extends queryi {
	
	private static $instance;
	
	public function json( $str )
	{
	
		header('Content-Type: application/json');
		print json_encode ( $str );
		exit();
		
	}
	
	function editLink( $link = '')
	{
		if (!$_SESSION['auth']) return false;
		print sprintf ('data-sq-edit="%s"', $link);
	}
	
	function getSlides( $page_id )
	{
		return self::getResultset( sprintf ("SELECT * FROM site_slides WHERE page_id = %d ORDER BY orderby DESC, id DESC", $page_id) );
	}
	
	function shareLink( $src, $url )
	{
		return sprintf ('<li><a href="%s" class="sq-social-icon sq-social-icon-%s"><span class="">Share on %2$s</span></a></li>', $url, $src);
	}
	
	function shareBar( $url = '', $title = '', $description = '', $options = '' )
	{
		if (!is_array( $options )) $options = array();
		$defaults = $options['defaults'];
		
		$url = urlencode($url);
		$title = urlencode($title);
		$description = urlencode($description);
		
		$return = '<ul class="sharebar">';
		if (!$defaults || $defaults['facebook']) 	$return .= self::shareLink ( 'facebook', sprintf ("http://www.facebook.com/sharer.php?u=%s&amp;t=%s", $url, $title, $description));
		if (!$defaults || $defaults['twitter']) 		$return .= self::shareLink ( 'twitter', sprintf ("https://twitter.com/share?url=%s&amp;text=%s", $url, $title, $description));
		if (!$defaults || $defaults['google']) 		$return .= self::shareLink ( 'google-plus', sprintf ("https://plus.google.com/share?url=%s", $url, $title, $description));
		if (!$defaults || $defaults['pinterest']) 	$return .= self::shareLink ( 'pinterest', sprintf ("http://pinterest.com/pin/create/button/?url=%s&amp;description=%s&amp;media=", $url, $title, $description));
		if (!$defaults || $defaults['linkedin']) 	$return .= self::shareLink ( 'linkedin', sprintf ("http://linkedin.com/shareArticle?mini=true&amp;url=%s&amp;title=%s", $url, $title, $description));
		if (!$defaults || $defaults['tumblr']) 		$return .= self::shareLink ( 'tumblr', sprintf ("http://www.tumblr.com/share/link?url=%s&amp;name=%s&amp;description=%s", $url, $title, $description));
		if (!$defaults || $defaults['email']) 		$return .= self::shareLink ( 'email', sprintf ("mailto:?body=%s&amp;subject=%s", $url, $title, $description));
		$return .= '</ul>';
		return $return;
	}
	
	function sqgslider( $slides='', $id='', $page_id = '' )
	{
		
		if (!is_array($slides) || empty($slides)) return false;
		?>
		<div class="slideshow"<?php if ($id) print sprintf (' id="%s"', $id); ?>>
			
			<?php foreach ($slides as $slide){ 
			if($slide['line_2']=="ONLINE MADE EASY")
			{
				$slide['line_2'] = "Online made <span data-typer-targets='{\"targets\" : [\"easy\",\"good\",\"effective\",\"friendly\",\"accessible\"]}'>easy</span>";
			}
			?>
			<div class="slide" <?php controller::editLink( sprintf ('/site/admin/slides.php?page_id=%d&id=%d', $page_id, $slide['id']) ); ?>>
				<img src="/site/userfiles/Image/slides/<?php print $slide['image']; ?>" alt="" class="slidebg">
				<div class="slidecontent">
					<div class="slideContentPositioner">
						<p class="line_1"><?php print $slide['line_1']; ?></p>
						<p class="line_2"><?php print $slide['line_2']; ?></p>
						
						<?php if ($slide['url']){ ?>
						<a href="" class="sqgSliderBtn"><?php print $slide['button_text'] ? $slide['button_text'] : 'More info'; ?> <span class="glyphicon glyphicon-share-alt"></span></a>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
			
		</div>
		<?php
		
	}
	
	function getArticles ( $options = '' )
	{
		if (!is_array($options)) $options = array();
		
	}
	
}