<?php
/**
*********************************************************
	SETUP THE PAGE	
*********************************************************
*/
include_once ("../../cms/lib/globals/config.php");
include_once ("../../cms/admin/_lib/globals/config.php");
include_once (ADMIN_GLOBALS_DIR . "setup.php");


$dbfunc = new db_functions;
$dbfunc->table = 'cottages';
$dbfunc->page_title = 'Cottages';
$dbfunc->title = 'cottage';
# $dbfunc->timestamps = false;
#$dbfunc->temp_dir = USERFILES . 'Image/';
//$dbfunc->active_field = 'active';
$dbfunc->table_description = array(
	'team_id'=>array('type'=>'hidden'),
	'cottage_url'=>array('type'=>'hidden'),
	'cottage_representative_photo'=>array(
		'type'=>'image',
		'location'=>USERFILES . 'Image/cottages/',
		'max_width'=>1920,
		'max_height'=>1080
	),
	'imagename'=>array('type'=>'image', 'location'=>ROOT_DIR.'/site/userfiles/Image/team/','crop_width'=>351,'crop_height'=>351),	
);
$dbfunc->list_display = array(
	'cottage_name'=>true,
	'slides'=>array('title'=>'Slides', 'default'=>'Slides', 'dynamic_url'=>'cottage-slides.php?cottage_id='),
	'images'=>array('title'=>'Photos', 'default'=>'Photos', 'dynamic_url'=>'cottage-photos.php?cottage_id='),
	'cottage_active'=>true,
#	'news_display_from'=>array('title'=>'display from'),
#	'news_display_until'=>array('title'=>'display until'),
#	'preview'=>array('title'=>'Preview', 'dynamic_url'=>'/news/###news_url###', 'default'=>'<span class="glyphicon glyphicon-eye-open"></span>'),
#	'display_on_home_page'=>array('title'=>'Home page?'),
);
/*
$dbfunc->mass_uploads = array(
	'remote_table'=>'demo_gallery_items',
	'filename_field'=>'filename',
	'unique_id_field'=>'gallery_id',
	'location'=>ROOT_DIR . 'uploads/'
);
*/
#$dbfunc->_default_orderby = ' orderer DESC';
$dbfunc->drag_to_sort = 'cottage_orderby';
# $dbfunc->edit = false;
#$dbfunc->sortable = true;
$dbfunc->pauseRedirect = true;
$dbfunc->_run();
if ($_POST){
	$db = new querytools;
	$db->_sql = "
		SELECT cottage_name AS t, CONCAT('/our-cottages/', cottage_url) AS u
		FROM cottages
		WHERE cottage_active = 1
		ORDER BY cottage_orderby DESC
	";
	$cottages = $db->getResultset();
	$db->_unset();
	$db->_sql = sprintf ("
		UPDATE content_pages
		SET child_menu = '%s'
		WHERE id = 7
	",
		str_replace ("'", "\'", json_encode($cottages))
	);
	$db->update();
	include ('../../cms/admin/_lib/classes/menus.php');
	$menus = new menus;
	$menus->updateMenus();
	
	$dbfunc->pauseRedirect = false;
	$dbfunc->_redirect();
}


/**
*********************************************************
	START PAGE OUTPUT
*********************************************************
*/

include_once (ADMIN_HEADER_PAGE); ?>
<style>
</style>
<?php
print $dbfunc->display;
include_once (ADMIN_FOOTER_PAGE);