<?php
/**
*********************************************************
	SETUP THE PAGE	
*********************************************************
*/
include_once ("../../cms/lib/globals/config.php");
include_once ("../../cms/admin/_lib/globals/config.php");
include_once (ADMIN_GLOBALS_DIR . "setup.php");


$dbfunc = new db_functions;
$dbfunc->table = 'site_team';
$dbfunc->page_title = 'Team members';
$dbfunc->title = 'team member';
# $dbfunc->timestamps = false;
$dbfunc->temp_dir = USERFILES . 'Image/';
//$dbfunc->active_field = 'active';
$dbfunc->table_description = array(
	'team_id'=>array('type'=>'hidden'),
	'imagename'=>array('type'=>'image', 'location'=>ROOT_DIR.'/site/userfiles/Image/team/','crop_width'=>351,'crop_height'=>351),	
);
$dbfunc->list_display = array(
	'name'=>true,
	'imagename'=>true,
#	'news_display_from'=>array('title'=>'display from'),
#	'news_display_until'=>array('title'=>'display until'),
#	'preview'=>array('title'=>'Preview', 'dynamic_url'=>'/news/###news_url###', 'default'=>'<span class="glyphicon glyphicon-eye-open"></span>'),
#	'display_on_home_page'=>array('title'=>'Home page?'),
);
/*
$dbfunc->mass_uploads = array(
	'remote_table'=>'demo_gallery_items',
	'filename_field'=>'filename',
	'unique_id_field'=>'gallery_id',
	'location'=>ROOT_DIR . 'uploads/'
);
*/
$dbfunc->_default_orderby = ' orderer DESC';
$dbfunc->drag_to_sort = 'orderer';
# $dbfunc->edit = false;
#$dbfunc->sortable = true;
$dbfunc->_run();


/**
*********************************************************
	START PAGE OUTPUT
*********************************************************
*/

include_once (ADMIN_HEADER_PAGE); ?>
<style>
</style>
<?php
print $dbfunc->display;
include_once (ADMIN_FOOTER_PAGE);