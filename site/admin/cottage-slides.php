<?php
/**
*********************************************************
	SETUP THE PAGE	
*********************************************************
*/
include_once ("../../cms/lib/globals/config.php");
include_once ("../../cms/admin/_lib/globals/config.php");
include_once (ADMIN_GLOBALS_DIR . "setup.php");

$cottage_id = sprintf ("%d", $_GET['cottage_id']);
$db = new querytools;
$db->_sql = sprintf ("
	SELECT cottage_name
	FROM cottages
	WHERE cottage_id='%s'
",
	$cottage_id 
);
$cottage = $db->nextrecord();
$details = explode ("-", $cottage['cottage_name']);
$page_title = $details[0];

$dbfunc = new db_functions;
$dbfunc->table = 'site_slides';
$dbfunc->title = 'cottage header slide';
$dbfunc->page_title = sprintf ('\'%s\' web page header slides', $page_title);
$dbfunc->temp_dir = USERFILES . 'Image/slides/'; # FOR USE WITH AWS FEATHER EDITOR
$dbfunc->new_item_url = '?id=new&cottage_id=' . $cottage_id;
# $dbfunc->timestamps = false;
$dbfunc->table_description = array(
	'id'=>array('type'=>'hidden'),
	'page_id'=>array('type'=>'hidden'),
	'cottage_id'=>array('type'=>'hidden', 'default'=>$cottage_id, 'value'=>$cottage_id),
	'line_1'=>array('editor'=>false),
	'url'=>array('help'=>'(Optional) The page location or web address where you want this slide to link to. If you want to link to a page on this website then simply copy the Page path from the respective Page info record'),
	'image'=>array('type'=>'image', 'location'=>USERFILES . 'Image/slides/', 'crop_width'=>1920, 'crop_height'=>930),
	
	/*'optional_product_link'=>array(
		#'type'=>'ignore',
		'fkey'=>'site_products.name',
		'fkeyconcatedwith'=>'!site_products.code',
		'fkeyq'=>'WHERE site_products.code!=""',
		'addlocked'=>true,
		#, 'multiple'=>true
	),
	'optional_category_link'=>array(
		#'type'=>'ignore',
		'fkey'=>'site_categories.title',
		#, 'multiple'=>true
	),
	'use_as_default_slide'=>array(
		'type'=>'ignore',
		'help'=>'Setting this will allow this particular slide to be used on any page which does not have it\'s own slide/s.'
	),
	*/
);
$dbfunc->list_display = array(
	'image'=>true,
	'line_1'=>true,
);
$dbfunc->extra_query = sprintf (" page_id='%d'", $page_id);
$dbfunc->mass_uploads = array(
	
);
$dbfunc->drag_to_sort = 'orderby';
# $dbfunc->edit = false;
#$dbfunc->sortable = true;
$dbfunc->_run();


/**
*********************************************************
	START PAGE OUTPUT
*********************************************************
*/

include_once (ADMIN_HEADER_PAGE);
?>
<style>
.col_use_as_default_slide {width: 80px; text-align: center; }
.btn-add-new {margin: 0 0 20px 0; }
</style>
<?php print $dbfunc->display; ?>

<?php
include_once (ADMIN_FOOTER_PAGE);