<?php if (!defined("ROOT_DIR")) die(); ?>

<style>
.admin-intro {margin-bottom: 30px; }
</style>

<div class="panel panel-primary ">
  <div class="panel-heading">
    <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Welcome <?php print ucfirst($_SESSION['auth']['firstname']); ?></h3>
  </div>
  <div class="panel-body">
    <p><strong>Welcome <?php print ucfirst($_SESSION['auth']['firstname']); ?>, you're now logged in to your admin/ website CMS.</strong></p><p>Your website consists of both the admin area AND the actual website.</p>
<?php if ($_SESSION['auth']['flags']){ ?>
<p>To edit page content (text, images, links, downloads etc) which is directly within a web page, now you're logged in, simply <a href="/" target="_blank"><span class="glyphicon glyphicon-share"></span> <strong>browse your website</strong></a> and click on a particular bit of text: a text editor should appear around the whole editable block. Make the necessary changes and simply click on <strong>Save</strong> at the top left of the page.</p>
<?php } ?>
  </div>
</div>
<?php if ($_SESSION['auth']['flags']){ ?>
<div class="row">
    <div class="col-md-4">
		<a href="/" class="panel panel-primary" target="_blank">
			<div class="panel-heading">
				On page editing
				<span class="glyphicon glyphicon-pencil"></span>
			</div>
			<div class="panel-body">
				<p><strong>Back to the website. </strong>This simply takes you back to the website so you can edit any page content.</p>
			</div>
		</a>
	</div>    
    <div class="col-md-4">
		<a href="/cms/admin/pages.php" class="panel panel-primary">
			<div class="panel-heading">
				Web pages
				<span class="glyphicon glyphicon-globe"></span>
			</div>
			<div class="panel-body">
				<p><strong>Administer all your web pages from here</strong> (To update a menu/ navigation, please visit the Menus section).</p>
			</div>
		</a>
	</div>
	<div class="col-md-4">
		<a href="/site/admin/config.php" class="panel panel-primary">
			<div class="panel-heading">
				General configuration
				<span class="glyphicon glyphicon-cog"></span>
			</div>
			<div class="panel-body">
				<p><b>Your user ID's</b> Site title/ Facebook/ Twitter/ Email and so on. All in here.</p>
			</div>
		</a>
	</div>
</div>
<div class="row">
    <div class="col-md-4">
		<a href="/cms/admin/menus.php" class="panel panel-primary">
			<div class="panel-heading">
				Menus/ Navigation
				<span class="glyphicon glyphicon-th"></span>
			</div>
			<div class="panel-body">
				<p><strong>Add pages and links to your page menus.</strong></p>
			</div>
		</a>
	</div>    
</div>
<?php } ?>
