$.fn.sqgslider = function( options ){
	
	defaults = {
		pauseDuration: 3000,
		animationDuration: 500,
		animationType: 'fade'
	}
	var defaults = $.extend( {}, defaults, options );
	var container = $(this), slides = $(container).find('> .slide'), current = -1, next = 0, btnContainer, total = slides.length, buttons = [], animation;
	var btnNext, btnPrevious;
	
	switch(defaults.animationType){
		case 'slide':
			break;
		default:
			$(slides).css({ 'display': 'none' });
			break;
	}
	
	var animate = function ( options ){
		clearInterval(animation);
		options = $.extend( {}, options );
		var items = $(slides[next]).find('> .slidecontent .container > *').css({ scale: [1.1, 1.1], 'opacity': 0, y: -30 });
		$(slides[next]).fadeIn( defaults.animationDuration, null, function(){
			var delay = 0;
			$(items).each(function (i, el){
				//$(el).delay(delay).fadeIn();
				$(el).delay(delay).transition({
					scale: [1, 1],
					y: 0,
					opacity: 1
				}, 1500);
				delay += 300;
			});
		});
		$(slides[current]).fadeOut();
		$(buttons).each(function(i, el){
			if (i==next){
				$(el).addClass('active');
			}else{
				$(el).removeClass('active');
			}
		});
		if (total < 2) return false;
		current = next;
		next = ((next + 1) < total) ? next + 1 : 0;
		animation = setTimeout(animate, defaults.pauseDuration);
	}
	
	var addButtons = function (){
		var btn = $('<a/>', { class: 'sqgslider-btn' });
		btnContainer = $('<div/>', { class: 'sqgslider-buttons' });
		
		for(a=0; a<total; a++){
			buttons[a] = $(btn).clone().attr('data-id', a).click(function(e){
				next = $(this).attr('data-id'); animate(); e.preventDefault();
			}).appendTo(btnContainer);
			$(buttons[0]).addClass('active');
		}
		$(btnContainer).appendTo(container);
	}
	animate( slides.length ? true : false );
	if (total > 1) addButtons();
	
};

