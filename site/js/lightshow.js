/**
---------------------------------------------------------------------------
	Lightshow (photo galleries etc)
---------------------------------------------------------------------------
*/
var $w = $(window);
var pageWidth = $w.width();

$.fn.lightshow = function(options){
	
	if (!$(this).length) return false;
	
	$(this).each(function(i, lightshow){
	
	
		/* INIT */
		if ($(lightshow).playing) return false;
		$(lightshow).playing = true;
		
		
		if (!options) options = {};
		var fullView = $(lightshow).find('.lightshow-full');
		var links = $(lightshow).find('.lightshow-thumbs a');
		var duration = options.duration ? options.duration : 5000;
		var images = [];
		links.each(function(i, el){
			images[i] = $('<img/>', {src: $(el).attr('href') }).css({'display': 'none'}).appendTo($(fullView));
		});
		// alert (images.length);
	
		var total = links.length;
		var current = null;	
		var next = 0;
		var nextBtn = $('<a/>', {'class': 'lightshow-control lightshow-next', html: '<span class="glyphicon glyphicon-chevron-right"></span>'}).appendTo($(this).find('.lightshow-thumbs')).click(function(e){slide(null, 'next'); e.preventDefault(); });
		var prevBtn = $('<a/>', {'class': 'lightshow-control lightshow-prev', html: '<span class="glyphicon glyphicon-chevron-left"></span>'}).appendTo($(this).find('.lightshow-thumbs')).click(function(e){slide(null, 'previous'); e.preventDefault(); });
		
		/* SLIDE TO NEXT/ PREV/ ID */
		var slide = function(id, direction, options){
			if (id){
				next = id;
			}else{
				/* assume Next -> */
				if (current == null) {
					next = 0;
				}else{
					switch (direction){
						case 'previous':
							next = (next - 1 < 0) ? total - 1 : next - 1;
							break;
						default:
							next = (next + 1 >= total) ? 0 : next + 1;
							break;
					}
				}
			}
			images[next].fadeIn();
			$(fullView).animate({'height': $(images[next]).height() });
			if (current) images[current].fadeOut();
			current = next;
			$(links).removeClass('active');
			$(links[current]).addClass('active');
			if (id || direction){
				clearInterval(start);
				start = setInterval(slide, duration);
			}
		}
		
		$(links).each(function(i, el){
			$(el).click(function(e){
				slide(i);
				e.preventDefault();
			});
		});
		
		
		$(fullView).hammer().on('swipeleft', function(ev){
			slide();
			return false;
		}).on('swiperight', function(ev){
			slide(null, 'previous');
			return false;
		});
		
		/*
		$(fullView).swipe({
			//Generic swipe handler for all directions
			swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
				// $(this).text("You swiped " + direction ); 
				switch(direction){
					case 'right':
						slide(null, 'previous');
						break;
					case 'left':
						slide();
						break;
					default:
						return false;
						break;
				}
			},
			//Default is 75px, set to 0 for demo so any distance triggers swipe
			threshold:0
		});
		*/			
		
		/* START ANIMATION */
		slide();
		var start = setInterval(slide, duration);
	});
}

