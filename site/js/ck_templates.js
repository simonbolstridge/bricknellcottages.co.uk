var lorem = 'Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content. It\'s also called placeholder (or filler) text. It\'s a convenient tool for mock-ups.';

CKEDITOR.addTemplates("default",{
	imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),
	templates:[
		/* TWO COLUMNS, WITHIN A ROW (MOBILE FRIENDLY) */
		{
			title:"Two equal columns",
			image:"two-columns.gif",
			description:"Two block columns of equal width (mobile friendly)",
			html:'<div class="row"><div class="col-sm-6"><p>Column 1</p></div><div class="col-sm-6"><p>Column 2</p></div></div>'
		},
		/* TWO COLUMNS, WITHIN A ROW (MOBILE FRIENDLY) */
		{
			title:"Two-thirds/ One-third columns",
			image:"two-columns.gif",
			description:"Two columns (2/3, 1/3 widths) (mobile friendly)",
			html:'<div class="row"><div class="col-sm-8"><p>Column 1</p></div><div class="col-sm-4"><p>Column 2</p></div></div>'
		},
		/* TWO COLUMNS, WITHIN A ROW (MOBILE FRIENDLY) */
		{
			title:"One-third/ Two-thirds columns",
			image:"two-columns.gif",
			description:"Two columns (1/3, 2/3 widths) (mobile friendly)",
			html:'<div class="row"><div class="col-sm-4"><p>Column 1</p></div><div class="col-sm-8"><p>Column 2</p></div></div>'
		},
		/* IMAGE BLOCK, WITH IMAGE CAPTION, FLOATED RIGHT */
		{
			title:"Image and caption, on right of page",
			image:"image-and-caption-right.gif",
			description:"Image placeholder, with positioned image caption (on right of container)",
			html:'<div class="pull-right"><img src="/site/img/cms-placeholder.jpg" alt="Image placeholder"><p class="img-block-caption">Image caption</p></div><p>' + lorem + '</p>'
		},
		/* IMAGE BLOCK, WITH IMAGE CAPTION, FLOATED LEFT */
		{
			title:"Image and caption, on left of page",
			image:"image-and-caption-left.gif",
			description:"Image placeholder, with positioned image caption (on left of container)",
			html:'<div class="pull-left"><img src="/site/img/cms-placeholder.jpg" alt="Image placeholder"><p class="img-block-caption">Image caption</p></div><p>' + lorem + '</p>'
		},
		
		/*
		{
			title:"Image and Title 2",
			image:"template1.gif",
			description:"One main image with a title and text that surround the image.",
			html:'<div class="image-block"><img src="/userfiles/Image/placeholder.jpg" alt="Image placeholder" /><span class="image-caption">Image caption goes here</span></div>'
		},
		{
			title:"Image and Title",
			image:"template1.gif",
			description:"One main image with a title and text that surround the image.",
			html:'<h3><img src=" " alt="" style="margin-right: 10px" height="100" width="100" align="left" />Type the title here</h3><p>Type the text here</p>'
		},
		{
			title:"Strange Template",
			image:"template2.gif",
			description:"A template that defines two colums, each one with a title, and some text.",
			html:'<table cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr><td style="width:50%"><h3>Title 1</h3></td><td></td><td style="width:50%"><h3>Title 2</h3></td></tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></table><p>More text goes here.</p>'
		},
		{
			title:"Text and Table",
			image:"template3.gif",
			description:"A title with some text and a table.",
			html:'<div style="width: 80%"><h3>Title goes here</h3><table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1"><caption style="border:solid 1px black"><strong>Table title</strong></caption><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table><p>Type the text here</p></div>'
		}
		*/
	]
});