(function(){
	$(document).ready(function(e) {
		
		var mobile;
		
		var headerHt = $('header.header').outerHeight();
		var menuHt = $('nav.navigation').outerHeight();
		
		
		$('.nav-opener').each(function(i, opener){
			var nav = $('.navigation');
			var menuWidth;
			var findMenuWidth = function(){
				menuWidth = ($(nav).outerWidth());
				if ($('.nav-opener').css('display') == 'block'){
					mobile = true;
				} else mobile = false;
				
				$(window).unbind('scroll');
				
				if (!mobile){
					$(window).scroll(function(e){
						if ($(document).scrollTop() > headerHt){
							$('body').addClass('scrolled');
							$('header.header').css({'margin-bottom': menuHt});
						}else{
							$('body').removeClass('scrolled');
							$('header.header').css({'margin-bottom': 0});
						}
					});
				}
			};
			
			if (location.hash){
				$('html, body').animate({
					scrollTop: $(location.hash).offset().top - menuHt - 20
				}, 1500);
			}
			
			$(window).resize(function(){
				findMenuWidth();
			});
			findMenuWidth();
			$(nav).find('li.active > a > .glyphicon-chevron-down, li.active-child > a > .glyphicon-chevron-down').toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-up')
			$(opener).click(function(e){
				e.preventDefault()
				if ($(nav).hasClass('openNav')){
					$(nav).removeClass('openNav');
					$(nav).animate({'right': -menuWidth});
				}else{
					$(nav).addClass('openNav');
					$(nav).animate({'right': 0});
				}
			});
			$(nav).hammer().on('swiperight', function(ev){
				$(nav).removeClass('openNav');
				$(nav).animate({'right': -menuWidth});
			});
			$(nav).find('.glyphicon-chevron-down, .glyphicon-chevron-up').click(function(e2){
				var icon = $(this);
				var li = $(icon).parent().parent();
				$(icon).toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-up');
				var child = $(li).find('ul');
				$(child).slideToggle();
				e2.preventDefault();
			});
		});
		
		
		
		
		/*
		if ($('.nav-opener').css('display') == 'block'){
			// MOBILE NAVIGATION
			//$("div").transition({ x: 200 });
			$('.nav-opener').click(function(e){
				var opener = $(this);
				var nav = $('nav#mobile-navigation');
				var navWidth = $(nav).outerWidth();
				if ($(opener).hasClass('open')){
					$(opener).removeClass('open');
					// $(nav).animate({'right': -navWidth});
					$('.container, .sqg-slider, #mobile-navigation').transition({ x: 0 });
				}else{
					$(opener).addClass('open');
					//$(nav).animate({'right': 0});
					$('.container, .sqg-slider, #mobile-navigation').transition({ x: -navWidth });
				}
			});
			$('nav#navigation .glyphicon').click(function(e){
				var glyph = $(this);
				var li = $(this).parent().parent();
				if ($(glyph).hasClass('glyphicon-chevron-down')){
					$(glyph).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
				}else{
					$(glyph).addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
				}
				if ($(li).find('> .drop-down').size()){
					var ul = $(li).find('> .drop-down > ul');
				}else{
					var ul = $(li).find('> ul');
				}
				$(ul).slideToggle();
				e.preventDefault();
			});
		}
		*/
		
		
		var pageWidth;
		var setPageWidth = function(){
			pageWidth = $(window).width();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		$('form').submit(function(e){
			var submitBtns = $(this).find('button[type=submit], button[role=submit]');
			if (!submitBtns.hasClass('sending')){
				submitBtns.html('<span class="glyphicon glyphicon-refresh gly-spin"></span> Sending');
				submitBtns.addClass('sending');
			}
			e.preventDefault();
			// validate
		})
		
		
		
		
		$.fn.slider = function(options){
			if (!options) options = [];
			var container = $(this);
			var slides = $(container).find('.slide');
			var total = slides.length;
			var current = false;
			var next = 0;
			var interval = options.interval ? options.interval : 5000;
			var nextBtn, previousBtn, timer;
			slides.css({'display': 'none'});
			/*$(container).find('.block').each(function(i, block){
				$(block).each(function(j, $item){
					$($item).attr('data-sr', 'wait ' + (i * 0.4) + 's move 40px reset');
				});
			});*/
			
			var nextSlide = function(id, direction){
				clearTimeout(timer);
				if (id) {
					next = id;
				}
				if (direction){
					direction = -1500;
				}else{
					direction = 1500;
				}
				if (current !== false){
					$(slides[current]).fadeOut();
					//$(slides[current]).find('.block').animateBlock(1);
					$(slides[current]).find('.slide-blurb .container').transition({x: -direction}, 1000);
				}
				$(slides[next]).fadeIn();
				$(slides[next]).find('.slide-blurb .container').css({x: direction}).transition({x: 0}, 1000);
				current = next;
				next = next + 1 >= total ? 0 : next + 1;
				if (total > 1) timer = setTimeout(nextSlide, interval);
			}
			if (total > 1){
				nextBtn = $('<span/>', {class: 'glyphicon glyphicon-chevron-right sliderBtn sliderNextBtn'}).appendTo(container, 'top').click(function(e){
					id = next;
					nextSlide(id);
				});
				previousBtn = $('<span/>', {class: 'glyphicon glyphicon-chevron-left sliderBtn sliderPreviousBtn'}).appendTo(container, 'top').click(function(e){
					id = current - 1 < 0 ? total - 1 :  current - 1;
					nextSlide(id, 1);
				});
			}
			nextSlide();
		}
		$('.animated-header').slider();		
		

		
	});
})(jQuery);