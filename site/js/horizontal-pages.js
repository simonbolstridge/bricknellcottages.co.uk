(function(){
	/*
	------------------------------------------------------------
				HORIZONTAL SCROLLING PAGES
				Dependencies:
				Hammer (hammer-v2.0.3.min.js)
	------------------------------------------------------------
	*/
	$(document).ready(function(e) {
		var offset = $('#header').outerHeight();
		var scrollPage = function(target, parent, direction, noScrollTo){
			if (parent)
			{
				// alert ($(parent).attr('class'));
				//$w = $(window).width();
				//current_margin_left = parseInt($(parent).css('margin-left'));
				//alert(current_margin_left);
				children = $(parent).find('.page');
				current = 0;
				$(children).each(function(j, child){
					if ($(child).hasClass('current'))
					{
						current = j;
					}
				});
				switch(direction)
				{
					case 'left':
						if (current + 1 < children.length)
						{
							$(children).removeClass('current');
							$(children[current+1]).addClass('current');
							position = $(children[current+1]).position().left;
							target = '#' + $(children[current+1]).attr('id');
							$(parent).animate({'margin-left': -position}, 400, 'swing');
						} else return false;
						break;
					case 'right':
						if (current - 1 >= 0)
						{
							$(children).removeClass('current');
							$(children[current-1]).addClass('current');
							position = $(children[current-1]).position().left;
							target = '#' + $(children[current-1]).attr('id');
							$(parent).animate({'margin-left': -position}, 400, 'swing');
						} else return false;
						break;
				}
			}else{
				parent = $(target).parent();
				children = $(parent).find('.page').removeClass('current');
				$(target).addClass('current');
				position = $(target).position().left;
				$(target).parent().animate({'margin-left': -position}, 400, 'swing');
			}
			$('html, body').animate({
				scrollTop: $(parent).offset().top - $('#header').outerHeight()
			}, 500);
			// alert($(parent).offset().top);
			$('a[href*="#"]').removeClass('active');
			$('a[href*="' + target + '"]').addClass('active');
		}
		
		/* PAGE LOAD CHECKER */
		if (document.location.hash && $(document.location.hash).length)
		{
			if (!$(document.location.hash).parent().hasClass('horizontal-pages-inner')) return true;
			scrollPage(document.location.hash);
		}
		
		/* HORIZONTAL PAGE CHECKER */
		$('a[href*="#"]').click(function(e){
			var hash = $(this).attr('href').substr($(this).attr('href').indexOf("#"));
			if (!$(hash).parent().hasClass('horizontal-pages-inner')){
				$('html, body').animate({
					scrollTop: $(hash).offset().top - $('#header').outerHeight()
				}, 500);
				e.preventDefault();
				return false;
			}
			scrollPage(hash);
			e.preventDefault();
		});
		
		$('.horizontal-pages-inner').each(function(i, parent){
			if (!$(parent).find('.page.current').length){
				$($(parent).find('.page')[0]).addClass('current');
			}
			$(parent).hammer().on('swipeleft', function(ev){
				scrollPage(null, parent, 'left');
			}).on('swiperight', function(ev){
				scrollPage(null, parent, 'right');
			});
		});
		
		
	});	
	$(window).resize(function(e){
		/* REPOSITION SCROLLED HORIZONTAL-SCROLL PAGES */
		$('.horizontal-pages-inner').each(function(i, parent){
			var current = $(parent).find('.page.current');
			//scrollPage(current, null, null, true);
			position = $(current).position().left;
			$(parent).css({'margin-left': -position});
			
		});			
	});
	/*
	------------------------------------------------------------
				END HORIZONTAL SCROLLING PAGES
				Dependencies:
				Hammer (hammer-v2.0.3.min.js)
	------------------------------------------------------------
	*/
})(jQuery)