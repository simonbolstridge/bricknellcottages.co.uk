/*
----------------------------------------------------------------
			CKEditor styles
----------------------------------------------------------------
*/
CKEDITOR.stylesSet.add( 'styles', [
	
	{ name: 'Image on left',	 element: 'img', attributes: { 'class': 'img-left' } },
	{ name: 'Image on right', element: 'img', attributes: { 'class': 'img-right' } },	
	{ name: 'Image on left (border)',	 element: 'img', attributes: { 'class': 'img-left-border' } },
	{ name: 'Image on right (border)', element: 'img', attributes: { 'class': 'img-right-border' } },	
	
	{ name: 'highlight', element: 'p', attributes: { 'class': 'highlight' } },
	{ name: 'tagline', element: 'span', attributes: { 'class': 'tagline' } },
	{ name: 'captioned', element: 'div', attributes: { 'class': 'captioned' } },
	
	{ name: 'remove bullets', element: 'ul', attributes: { 'class': 'remove-bullets' } },
	{ name: 'Portrait holder', element: 'div', attributes: { 'class': 'circle-portrait' } },

		
	
	
	
	{ name: 'table-list', element: 'ul', attributes: { 'class': 'table-list' } },
	{ name: 'reasons list', element: 'ul', attributes: { 'class': 'reasons-list' } },
	
	{ name: 'button', element: 'a', attributes: { 'class': 'button' } },
	{ name: 'pdf', element: 'a', attributes: { 'class': 'pdf' } },
	{ name: 'underlined (large)', element: 'h2', attributes: { 'class': 'underlined' } },
	{ name: 'underlined (medium)', element: 'h4', attributes: { 'class': 'underlined' } }
	
	
	/*{ name: 'demo-section', element: 'div', attributes: { 'class': 'demo-section' } }*/
]);