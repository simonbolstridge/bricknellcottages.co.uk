(function(){
	var initialize = function () {	
		
		var myLatlng = new google.maps.LatLng(54.038357, -0.960390);	
		var markerLatLng = new google.maps.LatLng(54.038757, -0.960390);	
		
		var mapOptions = {
			center: myLatlng,
			zoom: 16,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			scrollwheel: false
		};
		var map = new google.maps.Map($('#gmap')[0], mapOptions);
		
		// var image = '/_lib/images/structure/marker.png';
		/*
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			// icon: image
		});
		*/
		
		
		var marker = new google.maps.Marker({
			position: markerLatLng,
			map: map,
			icon: '/site/img/mapmarker.png',
			title: 'Click to enlarge'
		});
		
		
		google.maps.event.addListener(marker, 'click', function() {
			window.open('https://maps.google.co.uk/maps?q=York+YO60+7RB&hl=en&sll=53.959564,-1.083728&sspn=0.001989,0.007113&hnear=York+YO60+7RB,+United+Kingdom&t=m&z=16');
		});	
	}
	$(document).ready(function(e) {
		
		if ($('#gmap').length) initialize();
	});
})(jQuery);
