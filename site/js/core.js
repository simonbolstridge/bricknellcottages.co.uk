(function(){
	$(document).ready(function(e) {

		$('.loginLink').click(function(e){
			$('.page-container').addClass('page-blur');
			$('.loginForm').css({'visibility' : 'visible'}).fadeIn();
			$('.loginForm input[type=text]').focus();
			e.preventDefault();
		});
		$('.loginForm .closeBtn').click(function(e){
			$('.page-container').removeClass('page-blur');
			$('.loginForm').fadeOut(null, null, function(){$('.loginForm'). css({'visibility' : 'hidden'}); });
			e.preventDefault();
		});
		
		$.fn.scrollto = function(){
			var element = $( this );
			var to = $(element).offset().top;
			to = to > 0 ? to - 120 : 0;
			$('html, body').animate({ scrollTop: to }, 1000);
		}
		$('nav.nav a').first().addClass('active');
		/*
		$('header h1 a, nav.nav a, a.btn').click(function(e){
			var hash = $(this).attr('href').split('#')[1];
			if ($(this).attr('href').split('#')[0] != document.location) return true;
			hash = hash ? '#' + hash : '#header';
			$( hash ).scrollto();
			$('header h1 a, nav.nav a').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		*/
		$('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if ($('header.header .container .col-right').hasClass('open')){
					$('.nav-opener').click();
				}
                return doscroll(target);
            }
        });
        function doscroll(target)
        {
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 120
                }, 500);
                return false;
            }
        }
		$('.about-us').each(function(i, el){
			var profiles = $(this).find('.staff-profile');
			$(profiles).each(function(j, profile){
				$(profile).mouseover(function(e){
					$(profiles).each(function(k, eachProfile){
						if (j != k){
							$(eachProfile).addClass('over');
						}else{
							$(eachProfile).removeClass('over');
						}
					});
				}).mouseout(function(e){
					$(profiles).removeClass('over');
				});
			});
		});
		
		$('input.switch').bootstrapSwitch({onText: 'Yes', offText: 'No', onColor: 'green', offColor: 'red', onSwitchChange: function(e, state){
			if (state != false){
				$('.project-info').slideDown();
			}else{
				$('.project-info').slideUp();
			}
		}});
		var pageWidth;
		var setPageWidth = function(){
			pageWidth = $(window).width();
		}
		$(window).resize(function(){
			setPageWidth();
		});
		setPageWidth();
		
		$('form.login').submit(function(e){
			var form = $(this);
			var data = $(form).serialize();
			$.ajax({
				type: 'post',
				data: data,
				success: function(response){
					if (response.success){
						window.location = response.redirect;
					} else {
						if ($(form).find('.note').length){
							$(form).find('.note').html(response.html);
						}else{
							$('<div/>', {class: 'note', html: response.html}).prependTo ($(form));
						}
					}
				}
			});
			e.preventDefault();
		});
		
		$.fn.caSlider = function( options ){
			
			var defaults = {
				animationDuration: 1000,
				pauseDuration: 3000
			};
			var container = $(this);
			defaults = $.extend({}, defaults, options );
			
			var timer;
			var slides = $(container).find('.caSlide');
			var blurbs = $(container).find('.caBlurb');
			var total = $(slides).length;
			slides.click(function(){
				animate();
			});
			
			// if (total < 2) return false;
			var current = -1, next = 0;
			switch (defaults.method){
				case 'slide':
					$(slides).css({ 'left': pageWidth });
					break;
				default:
					$(slides).css({ 'opacity': 0 });
					break;
			}
			$(blurbs).css({ 'opacity': 0, 'display': 'block', 'visibility': 'hidden' });
			
			var animate = function(){
				
				clearTimeout( timer );
				if (current >= 0){
					switch (defaults.method){
						case 'slide':
							$(slides[current]).transition({ 'left': -pageWidth, 'duration': defaults.animationDuration });
							$(slides[next]).css({ 'left': pageWidth }).transition({ 'left': 0, 'duration': defaults.animationDuration });
							$(blurbs[current]).animate({ 'opacity': 0 }, defaults.animationDuration, null, function(){ $(this).css({ 'visibility': 'hidden' }); });
							$(blurbs[next]).css({ 'visibility': 'visible', 'opacity': 0 }).animate({ 'opacity': 1 }, defaults.animationDuration);
							break;
						default:
							$(slides[current]).transition({ 'opacity': 0, 'duration': defaults.animationDuration });
							$(slides[next]).css({ 'opacity': 0 }).transition({ 'opacity': 1, 'duration': defaults.animationDuration });
							$(blurbs[current]).animate({ 'opacity': 0 }, defaults.animationDuration, null, function(){ $(this).css({ 'visibility': 'hidden' }); });
							$(blurbs[next]).css({ 'visibility': 'visible', 'opacity': 0 }).animate({ 'opacity': 1 }, defaults.animationDuration);
							break;
					}
				}else{
					// First time
					$(slides[next]).css({ 'left': 0, 'opacity': 0 }).transition({ 'opacity': 1, 'duration': defaults.animationDuration });
					$(blurbs[next]).css({ 'visibility': 'visible' }).transition({ 'opacity': 1, 'duration': defaults.animationDuration });
				}
				
				current = next;
				next = next + 1 >= total ? 0 : next + 1;
				
				if (total > 1) timer = setTimeout(function(){animate(); }, defaults.pauseDuration);
			}
			
			animate();
			
		}
		
		$('.caSlider').caSlider({ animationDuration: 750, pauseDuration: 3500 });
		
		$('.contact-form').parallax("20%", -0.2);
		//$('.caSlide .caImg').parallax("20%", -0.2);
		
		
		$.fn.portfolio = function( options ){
			
			var container = $(this);
			var items = $(container).find('.portfolio-item');
			var itemImgs = $(container).find('.portfolio-img');
			var itemBlurbs = $(container).find('.portfolio-blurb');
			
			var current = -1, next = 0, total = $(items).length;
			/*
			$(itemImgs).css({ 'left': pageWidth });
			$(itemBlurbs).css({ 'opacity': 0, 'visibility': 'hidden' });
			*/
			var animate = function(){
				
				if (current <= 0){
					// first
					
				} else {
					
				}
				
			}
			
		}
		$('.portfolio').portfolio();
		$('.nav-opener').click(function(e){
			var nav = $('header.header .container .col-right');
			var mvmt = $(nav).width();
			if ($(nav).hasClass('open')){
				$(nav).removeClass('open');
				//$('.page-container').transition({ x: -mvmt });
				
				$(nav).animate({ 'right': -pageWidth * 0.8 });
			}else{
				$(nav).addClass('open');
				//$('.page-container').transition({ x: 0 });
				$(nav).animate({ 'right': '0' });
			}
		});
		
	});
})(jQuery);