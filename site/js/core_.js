(function(){


	$(document).ready(function(e) {

		$(window).on("scroll", function() {
			if($(window).scrollTop() > 50) {
						$(".header").addClass("scrolled");
				} else {
						//remove the background property so it comes transparent again (defined in your css)
					 $(".header").removeClass("scrolled");
				}
		});
		

		/*
		---------------------------------------------------------
			PAGE TRANSITION EFFECT
		---------------------------------------------------------
		*/
		$('a').click(function(e){
			var url = $(this).attr('href');
			$('body').overlay();
			setTimeout(function(){ window.location = url; }, 500);
			e.preventDefault();
		});
		/*
		---------------------------------------------------------
			END PAGE TRANSITION EFFECT
		---------------------------------------------------------
		*/

		 
		
		
		/*
		---------------------------------------------------------
			SQG SOCIAL SHARER
		---------------------------------------------------------
		*/
			/*$(sharebar).find('a').each(function(j, a){
				$(a).click(function(e){
					var url = $(a).attr('href');
					bootbox.dialog({
						title: "Share this page",
						message: '<iframe class="sqg-share-iframe" src="' + url + '" frameborder="0"></iframe>'
					});
					e.preventDefault();
				});
			});*/
		/*
		---------------------------------------------------------
			END SQG SOCIAL SHARER
		---------------------------------------------------------
		*/
		
		
		/*
		---------------------------------------------------------
			HOME PAGE / TOP SLIDERS
		---------------------------------------------------------
		*/
		$('.slideshow').sqgslider({});
		/*
		---------------------------------------------------------
			END HOME PAGE / TOP SLIDERS
		---------------------------------------------------------
		*/
				
		
		/*
		---------------------------------------------------------
			SCROLLREVEAL
		---------------------------------------------------------
		*/
		window.sr = ScrollReveal();
		sr.reveal('.diamond', { delay: 500, scale: 0.9, easing : 'ease-in-out' }, 150);
		sr.reveal('.experts-block', { delay: 500, scale: 0.9, easing : 'ease-in-out' }, 150);
		/*
		---------------------------------------------------------
			END SCROLLREVEAL
		---------------------------------------------------------
		*/
		
		
		
		/*
		---------------------------------------------------------
			TYPER
		---------------------------------------------------------
		*/
		$('[data-typer-targets]').typer();
		/*
		---------------------------------------------------------
			END TYPER
		---------------------------------------------------------
		*/
		
		/*
		---------------------------------------------------------
			SCROLL PAGE / FIXED NAVIGATION
		---------------------------------------------------------
		*/
		/*
		$(window).scroll(function(e){
			if ($('body').scrollTop() > 80){
				$('body').addClass('scrolled');
			}else{
				$('body').removeClass('scrolled');
			}
		});
		*/
		/*
		---------------------------------------------------------
			END SCROLL PAGE / FIXED NAVIGATION
		---------------------------------------------------------
		*/
		
		
		
		
		
	
	});
})(jQuery);